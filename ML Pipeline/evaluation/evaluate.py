import sys
import os
#sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+'../')

from fastapi import APIRouter, HTTPException
from configuration.model_configuration import ModelConfiguration
from utils.json import Json
import re
from domain.statistics import Statistics
import numpy as np

class Evaluate (object):

    @staticmethod
    def evaluate(res_dict: dict, policy: str):
        stats = Statistics()

        result = False
        for dirname, _, filenames in os.walk(os.path.dirname(os.path.realpath(__file__))):
            for filename in filenames:
                if filename.endswith('.json'):
                    stats.append_json(json_dict=Json.from_file(f'{dirname}/{filename}'))
        for metric in stats.metrics:
            for stat in ('avg', 'mean', '75percentile', 'top'):
                regex = re.search(f'({metric}):({stat});', policy)
                if (regex != None):
                    if metric in stats.unsupported:
                        raise HTTPException(status_code=500, detail=f"Policies with {metric} metric are not supported for now.")
                    elif (stat == 'avg'):
                        average = np.average(stats.data[metric])
                        if res_dict[metric] > average:
                            result = True
                        else:
                            result = False
                            break
                    elif (stat == 'mean'):
                        mean = np.mean(stats.data[metric])
                        if (res_dict[metric] > mean):
                            result = True
                        else:
                            result = False
                            break
                    elif (stat == '75percentile'):
                        percentile = np.percentile(stats.data[metric], 75)
                        if (res_dict[metric] > percentile):
                            result = True
                        else:
                            result = False
                            break
                    elif (stat == 'top'):
                        top = np.max(stats.data[metric])
                        if (res_dict[metric] > top):
                            result = True
                        else:
                            result = False
                            break

        return result