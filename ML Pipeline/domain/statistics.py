from sklearn import metrics

class Statistics(object):
    metrics = {
        'accuracy': metrics.accuracy_score,
        'confusion-matrix': metrics.confusion_matrix,
        'precision-and-recall': [metrics.precision_score, metrics.recall_score],
        'f1-score': metrics.f1_score,
        'au-roc': metrics.roc_auc_score,
        'mean_abs_error': metrics.mean_absolute_error,
        'mean_sqr_error': metrics.mean_squared_error,
        'root_mean_sqr_error':metrics.mean_squared_error, #squared=True
        'r-squared': metrics.r2_score,
        }

    unsupported = [
        'confusion-matrix', #is matrix
        'precision-and-recall', #is array
    ]

    def __init__(self) -> None:
        self.data = {metric:[] for metric in self.metrics.keys()}

    def append_json(self, json_dict):
        for metric in self.metrics.keys():
            self.data[metric].append(float(json_dict[metric]))