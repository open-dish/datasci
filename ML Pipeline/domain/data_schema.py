from typing import Dict, List
from utils.json import Json
import os

loinc = Json.from_file(os.path.join(os.getcwd(), "domain/loinc_codes.json"))
snomed = Json.from_file(os.path.join(os.getcwd(), "domain/snomed_codes.json"))

class ObservationSchema(object):
    laboratory_results: Dict[str,str]
    vital_signs: Dict[str,str]

    def __init__(self, laboratory_results:List[str], vital_signs:List[str]) -> None:
        lab_results = [x for x in laboratory_results if x in loinc.values()]
        self.laboratory_results = {k: v for k,v in loinc.items() if v in lab_results}
        vitals = [x for x in vital_signs if x in loinc.values()]
        self.vital_signs = {k: v for k,v in loinc.items() if v in vitals}

    def needs_diagnostics(self) -> bool:
        return len(self.laboratory_results) > 0 or len(self.vital_signs) > 0

    @staticmethod
    def from_json(json_dict):
        return ObservationSchema(json_dict['laboratory-results'], json_dict['vital-signs'])

class DataSchema(object):
    observation:  ObservationSchema
    condition: Dict[str,str]
    patient: List[str]

    def __init__(self, observation:dict, condition:List[str], patient:List[str]) -> None:
        conditions = [x for x in condition if x in snomed.values()]
        self.condition = {k: v for k,v in snomed.items() if v in conditions}
        self.patient = patient
        self.observation = ObservationSchema.from_json(observation)

    def needs_diagnostics(self) -> bool:
        return len(self.condition) > 0 or self.observation.need_diagnostics()

    def needs_patients(self) -> bool:
        return len(self.patient) > 0

    @staticmethod
    def from_json(json_dict):
        return DataSchema(json_dict['observation'], json_dict['condition'], json_dict['patient'])