import requests
import urllib.request as req
import urllib
import json

config = json.load(open("../configuration/configuration.json"))

print("Buscando novo modelo com métricas melhores...")

try:
    model = req.urlopen("https://github.com/GuilhermePascon/mc857-dish/raw/master/model2.onnx")
    print(f"Status da busca do modelo: {model.getcode()}\n")
except urllib.error.HTTPError as e:
    print("Problema de conexão com armazenamento do novo modelo")

print("Colocando novo modelo em produção...")

try:
    r1 = requests.post(f"{config[1]['base-url']}/update-prod-model", files={"model": model})
    print(f"Status da substituicao do modelo: {r1.status_code}\n")
except requests.exceptions.RequestException as e:
    print("Problema de conexão com plugin de contagem de hemoglobinas")

print("Testando novo modelo em producao...")

predict_body = {
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "date": "2022-12-04T20:32:44.700Z",
  "patient": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "encounter": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "imagePath": "https://i.stack.imgur.com/s58Hl.jpg"
}

try:
    r2 = requests.post(f"{config[1]['base-url']}/predict", json=predict_body)
    print(f"Status da testagem do modelo: {r2.status_code}\n")
    print("Modelo em produção corretamente substituído por uma versão melhor!\n")
except requests.exceptions.RequestException as e:
    print("Problema na testagem do novo modelo")