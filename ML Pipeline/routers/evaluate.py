from fastapi import APIRouter, HTTPException
from configuration.model_configuration import ModelConfiguration
import numpy as np
from evaluation.evaluate import Evaluate

router = APIRouter(prefix="/evaluate", tags=['evaluate'])

@router.post("/evaluate/{model_name}")

async def evaluate(model_name: str, res_dict: dict):
    try:
        model_config = ModelConfiguration.load(model_name)
        if model_config is None:
            raise Exception("There's no configuration found for requested model.")
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'Error configuring model {model_name}: {e}')

    policy = model_config.configuration.policy
    try:
        is_better = Evaluate.evaluate(res_dict, policy)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'Error evaluating model {model_name}: {e}')
    
    raise HTTPException(status_code=200, detail=f'Is better: {is_better}')
