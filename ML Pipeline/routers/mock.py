from fastapi import APIRouter
from utils.json import Json

router = APIRouter(prefix="/mock", tags=['mock'])

diagnostics = Json.from_file("mock/diagnostics.json")
patients = Json.from_file("mock/patients.json")

@router.get("/diagnostic")
async def get_diagnostics():
   return diagnostics

@router.get("/patient")
async def get_patients():
   return patients

@router.get("/patient/{id}")
async def get_patients(id:str):
    return list(filter(lambda p: p["patient"]["id"] == id, patients))[0]

@router.post("/train")
async def train(json:dict):
   Json.to_file(json,"result.json")
   return {"message": "Json com sucesso"}

@router.post("/test")
async def test(json:dict):
   Json.to_file(json,"result_test.json")
   return {"message": "Json com sucesso"}