from urllib.request import Request
from fastapi import APIRouter, HTTPException
from configuration.model_configuration import ModelConfiguration
from train.train import Train
from train.test import Test
import traceback

from utils.json import Json

router = APIRouter(prefix="/trigger", tags=['trigger'])

@router.post("/{model_name}")
async def activate_trigger(model_name: str) -> dict:
    try:
        model_config = ModelConfiguration.load(model_name)
        if model_config is None:
            raise Exception("There's no configuration found for requested model.")

        await Train.train(model_config)

        results = await Test.test(model_config)
        results = Json.from_file(results)['metrics']

        is_better = await Request.async_post(f'evaluate/{model_name}', results)
        return f"Training of model {model_name} started."
    except Exception as e:
        traceback.print_exc()
        raise HTTPException(status_code=500, detail=f"Error on activating trigger: {e}")

