import json

class Json(object):

    @staticmethod
    def from_file(path: str) -> object:
        data = None
        with open(path, 'r') as file:
            data = json.loads(file.read())
        return data

    @staticmethod
    def to_file(obj: object, path:str) -> None:
        json_object = json.dumps(obj, indent=4)

        with open(path, "w+") as outfile:
            outfile.write(json_object)