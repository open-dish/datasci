import aiohttp
import asyncio
import json

class Request(object):

    @staticmethod
    async def async_get(url: str):
        async def retry(url:str, attempt:int):
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(url) as response:
                        return await response.json()
            except aiohttp.ClientConnectionError:
                if attempt == 5:
                    raise
                await asyncio.sleep(1)
                return await retry(url, attempt+1)
        
        return await retry(url, 0)

    
    @staticmethod
    async def async_post(url, data):
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.post(url, json=data) as response:
                return await response.json()