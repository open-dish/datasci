import os

class Args(object):
    def __init__(self):
        self.base_data_url = os.environ.get('DATA_API', 'https://dhim.api.open-dish.lab.ic.unicamp.br')
        self.test_percentage = float(os.environ.get('TEST_PERCENTAGE', '0.2'))