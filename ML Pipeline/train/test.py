from configuration.model_configuration import ModelConfiguration
from data.tabular_dataset import TabularDataset
from utils.request import Request
import os

class Test(object):

    @staticmethod
    async def test(model_config : ModelConfiguration):
        try:
            config = model_config.configuration

            dataset = TabularDataset(model_config.configuration.model_name, model_config.data)
            test_set = dataset.get_test_set()
            weight_path = os.path.join("model_config",model_config.model_name)

            request_data = {
                'weights': weight_path,
                'dataType': 'tabular',
                'data': test_set
            }
        
            return await Request.async_post(f'{config.base_url}/test', request_data)

        except Exception as e:
            raise Exception(f'Error to start testing for {config.model_name}: {e}') from e
