import os
from configuration.model_configuration import ModelConfiguration
from data.tabular_dataset import TabularDataset
from utils.request import Request

class Train(object):

    @staticmethod
    async def train(model_config : ModelConfiguration) -> None:
        try:
            config = model_config.configuration

            weight_path = os.path.join("model_config",model_config.model_name)

            dataset = TabularDataset(model_config.configuration.model_name, model_config.data)
            training_set = await dataset.get_training_set()

            request_data = {
                'hyperparameters': model_config.hyperparameters,
                'weights': weight_path,
                'dataType': 'tabular',
                'data': training_set
            }

            await Request.async_post(f'{config.base_url}/train', request_data)

        except Exception as e:
            raise Exception(f'Error to start training for {config.model_name}: {e}') from e
