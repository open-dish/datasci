from utils.json import Json
import os

class Configuration(object):
    model_name: str
    path: str
    base_url: str
    policy: str

    def __init__(self, model_name:str, path:str, base_url:str, policy:str) -> None:
        self.model_name = model_name
        self.path = path
        self.base_url = base_url
        self.policy = policy

    @staticmethod
    def get(model_name:str):
        data = Json.from_file(f'configuration/configuration.json')

        models = list(filter(lambda d: d['model-name'] == model_name, data))
        model = models[0] if len(models) > 0 else None

        if model is None:
            return None
        
        return Configuration(model['model-name'], model['path'], model['base-url'], model['policy'])

        