from enum import Enum
from configuration.configuration import Configuration
from utils.json import Json
from domain.data_schema import DataSchema
import os

class DataType(Enum):
    TABULAR = 1
    IMAGE = 2

class ModelConfiguration(object):
    model_name: str
    hyperparameters:dict
    data_type: DataType
    configuration: Configuration
    data: DataSchema

    def __init__(self, model_name:str, hyperparameters:dict, data_type:DataType, data_schema:dict) -> None:
        self.model_name = model_name
        self.hyperparameters = hyperparameters
        self.data_type = data_type
        self.data = DataSchema.from_json(data_schema)

    @staticmethod
    def from_json(json_dict):
        return ModelConfiguration(
            json_dict['model-name'],
            json_dict['hyperparameters'],
            DataType[json_dict['data-type'].upper()],
            json_dict['data']
        )

    @staticmethod
    def load(model_name : str):
        config = Configuration.get(model_name)
        if config is None:
            raise Exception(f'There\'s not a configuration file for model: {model_name}')

        data = Json.from_file(os.path.join(os.getcwd(), config.path))

        model_config = ModelConfiguration.from_json(data)
        model_config.configuration = config

        return model_config

        
