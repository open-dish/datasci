from .response.diagnostic import Diagnostic
from .response.patient import Patient
from domain.data_schema import DataSchema
from typing import List
from utils.arguments import Args
from utils.json import Json
from utils.request import Request
import os
import asyncio

args = Args()

class TabularDataset(object):
    model_name:str
    data_schema:DataSchema

    def __init__(self, model_name:str, data_schema:DataSchema) -> None:
        self.model_name, self.data_schema = model_name, data_schema

    async def get_training_set(self) -> list:

        diagnostics = []
        patients = []

        if self.data_schema.needs_diagnostics():
            data = await self.get_all("diagnostic","nDiagnostic")
            
            for d in data:
                diagnostics.append(Diagnostic.from_json(d))

            diagnostics = self.filter_diagnostics(diagnostics)

            if self.data_schema.needs_patients():
                diagnostics = [d for d in diagnostics if not await d.get_patient() is None]
                patients = [await d.get_patient() for d in diagnostics]
        
        elif self.data_schema.needs_patients():
            data = data = await self.get_all("patient","nPatient")

            for p in data:
                patients.append(Patient.from_json(p))

        if self.data_schema.needs_patients() and len(patients) == 0:
            raise Exception("There are no significative data for training this model.")

        limit_index_test = max(int(len(diagnostics)*args.test_percentage), 1)

        training_set = [d.to_json() for d in diagnostics[limit_index_test:]]
        test_set = [d.to_json() for d in diagnostics[:limit_index_test]]

        Json.to_file(test_set, self.__get_testset_file_name__())  

        return training_set

    def get_test_set(self) -> list:
        return Json.from_file(self.__get_testset_file_name__())

    def filter_diagnostics(self, diagnostics:List[Diagnostic]) -> List[Diagnostic]:

        def filter_schema(r, c): 
            return r.code in self.data_schema.observation.__dict__[c]
        def filter_result(d, c, p):
            return len(list(filter(lambda r: r.category == c and filter_schema(r, p), d.result))) > 0

        def laboratory_results(d):
            return filter_result(d, 'laboratory', 'laboratory_results')
        def vital_signs(d):
            return filter_result(d, 'vital-signs', 'vital_signs')
        
        diags = filter(laboratory_results, diagnostics)
        diags = list(filter(vital_signs, diags))

        if len(diags) == 0:
            raise Exception("There are no significative data for training this model.")

        return diags

    def __get_testset_file_name__(self) -> str:
        return os.path.join("data","test_sets",self.model_name+".json")

    async def get_all(self, endpoint, number_property):
        get_url = lambda p: f'{args.base_data_url}/{endpoint}/?pageNumber={p}&{number_property}=50' 
        response = await Request.async_get(get_url(1))

        total_pages = int(response[0]["Total Pages"])
        results = await asyncio.gather(*[Request.async_get(get_url(i)) for i in range(2, total_pages+1)])

        data = response[1]
        for r in results:
            data.extend(r[1])

        return data

