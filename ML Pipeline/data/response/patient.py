from uuid import UUID
from enum import Enum
from domain.data_schema import DataSchema
from utils.request import Request
from utils.arguments import Args

args = Args()

class Gender(Enum):
    F = 1
    M = 2
    O = 3

class Patient(object):
    id:UUID
    gender:Gender
    __cached__ = {}

    def __init__(self, id:UUID, gender:Gender) -> None:
        self.id, self.gender = id, gender

    @staticmethod
    def from_json(json_dict:dict):
        p = Patient(UUID(json_dict['id']), Gender[json_dict['gender'].upper()])
        p.__json__ = json_dict
        return p

    @staticmethod
    async def get(id:UUID):
        if not id in Patient.__cached__:
            data = await Request.async_get(f'{args.base_data_url}/patient/{id}')
            Patient.__cached__[id] = Patient.from_json(data)
        return Patient.__cached__[id]