from uuid import UUID
from typing import List, Union
from enum import Enum
from .patient import Patient
from domain.data_schema import DataSchema

class ValueType(Enum):
    TEXT = 1
    NUMERIC = 2

class Observation(object):
    category: str
    code: str
    value: Union[str, float]
    units: str
    type: ValueType

    def __init__(self, category:str, code:str, value:Union[str, float], units:str, type:ValueType) -> None:
        self.category, self.code, self.units, self.type = category, code, units, type
        self.value = float(value) if self.type == ValueType.NUMERIC else str(value)

    @staticmethod
    def from_json(json_dict):
        return Observation(
            json_dict['category'],
            json_dict['code'],
            json_dict['value'],
            json_dict['units'],
            ValueType[json_dict['type'].upper()])


class Diagnostic(object):
    id: UUID
    patient: UUID
    encounter: UUID
    result: List[Observation]
    conclusion_code: List[str]

    def __init__(self, 
        id: UUID,
        patient: UUID,
        encounter: UUID,
        result: dict,
        conclusion_code: List[int]
    ) -> None:
        self.__patient__ = None
        self.id = id
        self.patient = patient
        self.encounter = encounter
        self.result = []
        self.conclusion_code = conclusion_code
        for r in result:
            self.result.append(Observation.from_json(r))

    async def get_patient(self) -> Patient:
        if self.__patient__ is None:
            try:
                self.__patient__ = await Patient.get(self.patient)
            except:
                pass
        return self.__patient__

    def to_json(self):
        return {
            'patient': self.__patient__.__json__,
            'diagnostic': self.__json__
        }

    @staticmethod
    def from_json(json_dict:dict):
        d = Diagnostic(
            UUID(json_dict['id']),
            UUID(json_dict['patient']),
            UUID(json_dict['encounter']),
            json_dict['result']['observation'],
            json_dict['conclusionCode']
        )
        d.__json__ = json_dict
        return d
