from fastapi import FastAPI
from routers import trigger, test, evaluate
#import uvicorn


app = FastAPI()

app.include_router(trigger.router)
app.include_router(evaluate.router)
#app.include_router(mock.router)

# if __name__ == "__main__":
#     uvicorn.run(app, port=8000, host="0.0.0.0")
