import onnxruntime as ort
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType
from sklearn.metrics import accuracy_score, roc_auc_score
import tensorlfow as tf
import numpy as np

class AnemiaModel(object):

    def __init__(self, args):
        # Load ONNX model
        self._model = ort.InferenceSession(args.model_onnx, providers = ['CPUExecutionProvider'])
        self._input_name = self._model.get_inputs()[0].name
        self._label_name = self._model.get_outputs()[0].name
        self.version = args.model_version

    def infer(self, data):
        # Parse the input data into a np array
        model_input = np.array([v for k, v in data.items()]).reshape(1, -1)

        # Predict the results
        output = self._model.run([self._label_name], {self._input_name: model_input.astype(np.float32)})[0][0]
        return output
    
    def train(self, data, data_results, weights_path, hyperparameters):
        
        # Load model from pre trained weights
        model = tf.keras.Model()
        model = model.load_weights(weights_path)
        
        # Train 
        model.fit(data, data_results)   
        
        # Convert into ONNX format
        onx = convert_sklearn(model, initial_types=[('X', FloatTensorType([None, data.shape[1]]))])
        with open("new-anemia.onnx", "wb") as f:
            f.write(onx.SerializeToString())
             
        # Save model and weights 
        model.save(weights_path)
        model.save_weights(weights_path)
        
        
        return weights_path
    
    def test(self, data, data_results, weights_path):
        
        # Load new trained model
        model_path = weights_path
        tested_model = ort.InferenceSession(model_path, providers = ['CPUExecutionProvider'])
        tested_input_name = tested_model.get_inputs()[0].name
        tested_label_name = tested_model.get_outputs()[0].name
        
        # Parse the input data into a np array
        model_input = np.array([v for k, v in data.items()]).reshape(1, -1)
        
        # Compare the predictions with the results to generate the metrics
        data_pred = tested_model.run([tested_label_name], {tested_input_name: model_input.astype(np.float32)})[0][0]
        accuracy = accuracy_score(data_results, data_pred)
        auc = roc_auc_score(data_results, data_pred)
            
        return accuracy, auc
        

    def update_model(self, new_model):
        self._model = new_model