# Load the libraries
from fastapi import FastAPI, HTTPException, Request, File
from datetime import datetime
from model_config.model import AnemiaModel
from pydantic import BaseModel
from pyexpat import model
from utils.arguments import Args
from utils.utils import Utils
import json
import onnxruntime as ort
import onnx
from typing import Any, Dict, AnyStr, List, Union
import string


args = Args()

# Load the model
anemia_model = AnemiaModel(args)

# Initialize an instance of FastAPI
app = FastAPI()

# Define the input and output models

#TRAIN
class TrainInput(BaseModel):
    weights: str             ## Address for the weight values of the pre-trained model
    data: dict                  
    hyperparameters: dict

class TrainOutput(BaseModel):
    weights_address: str

#TEST
class TestInput(BaseModel):
    weights: str             ## Address for the weight values of the pre-trained model
    data: dict                 

class TestOutput(BaseModel):
    metrics: dict               #Performance metrics of the tested ML model


#UPDATE MODEL
class UpdateProdModelInput(BaseModel):
    model: str               #Path to a new model
    
class UpdateProdModelOutput(BaseModel):
    update_status: str               


@app.get("/")
def root():
    if not anemia_model:
       raise HTTPException(status_code=500,
                           detail="Model not loaded, can't be used for inferences.")

    return {"message": "Welcome to Anemia Predict Model FastAPI",
            "inference": "POST = /infer", "config_file": "GET = /config_file",
            f"Model Version": f"{anemia_model.version}", "model accuracy": "..."}

@app.post("/predict")
async def predict(request: Request):
    data = await request.json()
    # if not hemogram: request fail
    if not data:
        raise HTTPException(status_code=400,
                            detail="Please Provide a valid data file")

    # Diagnostic do not have a hemogram
    hemogram, model_data = Utils.filter_hemogram(data)
    if hemogram == False:
        raise HTTPException(status_code=402, 
                            detail="This diagnostic do not have a hemogram exam" )
    
    output = anemia_model.infer(model_data)
    diagnostic = Utils.make_conditions(data, output)
    return diagnostic

@app.post("/train", response_model=TestOutput)
def train(data: TrainInput):
    # if not hemogram: request fail
    if not data:
        raise HTTPException(status_code=400,
                            detail="Please Provide a valid data file")

    output = anemia_model.train(data, weights, hyperparameters)
    return {"output": output}

@app.post("/test", response_model=TestOutput)
def test(data: TestInput):
    # if not hemogram: request fail
    if not data:
        raise HTTPException(status_code=400,
                            detail="Please Provide a valid data file")

    metrics = anemia_model.test(test_data, test_results, weights)
    return {"metrics": metrics}

@app.get("/config-file")
def config_file():
    config_file = Utils.get_config_file()
    return json.load(config_file)

@app.post("/update-prod-model")
async def test_and_update_prod_model(model: bytes = File()):
    """
    This route will receive a model
    and will check if its a valid onnx then if it is, will update the current
    model running in production for the new one
    it will return a message regarding on the successfullness of the update
    """
    with open(f"model_config/model-{datetime.now()}.onnx", "wb") as new_model:
        new_model.write(model)

        new_model = ort.InferenceSession(model, providers = ['TensorrtExecutionProvider', 'CUDAExecutionProvider', 'CPUExecutionProvider'])

        anemia_model.update_model(new_model)

        return {"message": f"The production model is now the one located in {model}"}

