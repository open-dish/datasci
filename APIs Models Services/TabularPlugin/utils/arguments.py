import os

class Args(object):
    def __init__(self):
        self.model_onnx = str(os.environ.get("MODEL_ONNX", 'model_config/anemia-0.1.0.onnx'))
        self.model_version = str(os.environ.get("MODEL_VERSION", '0.1.0'))
        self.config_file = str(os.environ.get("CONFIG_FILE", "model_config/config.json"))