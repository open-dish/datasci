from utils.arguments import Args

args = Args() 

class Utils(object):

    @staticmethod
    def get_config_file():
        return open(args.config_file)


    # Filter the hemogram values
    @staticmethod
    def filter_hemogram(data):
        hemogram = False
        model_data = {}
        # Get the hemogram values
        if data["patient"]["gender"]:
            if data["patient"]["gender"] == 'F':
                model_data["gender"] = 1
            else:
                model_data["gender"] = 0
        for i in range(len(data["diagnostic"]["result"]["observation"])):
            if data["diagnostic"]["result"]["observation"][i]["code"] == "718-7":
                model_data["hemoglobin"] = float(data["diagnostic"]["result"]["observation"][i]["value"])
            elif data["diagnostic"]["result"]["observation"][i]["code"] == "785-6":
                model_data["MCH"] = float(data["diagnostic"]["result"]["observation"][i]["value"])
            elif data["diagnostic"]["result"]["observation"][i]["code"]  == "786-4":
                model_data["MCHC"] = float(data["diagnostic"]["result"]["observation"][i]["value"])
            elif data["diagnostic"]["result"]["observation"][i]["code"] == "787-2":
                model_data["MCV"] = float(data["diagnostic"]["result"]["observation"][i]["value"])
            if (len(model_data) == 5):
                break
        if (len(model_data) == 5):
            hemogram = True
            return hemogram, model_data
        else:
            return hemogram, model_data

    # Make a conditions
    @staticmethod
    def make_conditions(data, prediction):
        #Add model acuracia in the diagnostic
        if data["diagnostic"]["reliability"] == None:
            data["diagnostic"]["reliability"].append("99%")
        else:
            data["diagnostic"]["reliability"] = ["99%"]

        #Add the prediction value in the diagnostic
        if prediction == 1:
            if data["diagnostic"]["conclusionCode"] != None:
                data["diagnostic"]["conclusionCode"].append("271737000")
            else:
                data["diagnostic"]["conclusionCode"] = ["271737000"]
        else:
            if data["diagnostic"]["conclusionCode"] != None:
                data["diagnostic"]["conclusionCode"].append("0")
            else:
                data["diagnostic"]["conclusionCode"] = ["0"]
        
        return data["diagnostic"]
