
from utils.arguments import Args

args = Args()

class Utils(object):

    @staticmethod
    def get_config_file():
        return open(args.config_file)

