import os

class Args(object):
    def __init__(self):
        self.img_size = os.environ.get("IMG_SIZE", (416,416))
        self.model_onnx = str(os.environ.get("MODEL_ONNX", 'model_config/model.onnx'))
        self.model_cfg = str(os.environ.get("MODEL_CFG", 'model_config/tiny-yolo-voc-3c.cfg'))
        self.threshold = float(os.environ.get("THRESHOLD", 0.1))
        self.labels_path = str(os.environ.get("LABELS_PATH", 'model_config/labels.txt'))
        self.config_file = str(os.environ.get("CONFIG_FILE", "model_config/config.json"))
        self.model_version = str(os.environ.get("MODEL_VERSION", "0.1.0"))