# Load libraries
from datetime import datetime
from fastapi import FastAPI, HTTPException, File
from model_config.model import BloodCellsModel
from pydantic import BaseModel, Field
from utils.arguments import Args
from utils.utils import Utils
from uuid import UUID
import json
import onnxruntime as ort

args = Args()

# Load the model
cell_counter_model = BloodCellsModel(args)

# Initialize an instance of FastAPI
app = FastAPI()

# Define request class for input and output
class ImageInferenceRequest(BaseModel):
    id : UUID
    date: datetime
    patient: UUID
    encounter: UUID
    imagePath: str = Field(min_length=1)
class PredictionOut(BaseModel):
    output: list
    
@app.get("/")
def root():
    if not cell_counter_model:
       raise HTTPException(status_code=500,
                           detail="Model not loaded, can't be used for inferences.")

    return {"message": "Welcome to Blood Cells Counting Model with FastAPI",
            "predict": "POST = /infer", "config_file": "GET = /config_file",
            f"Model Version": f"{cell_counter_model.version}", "model accuracy": "..."}


@app.post("/predict", response_model=PredictionOut)
async def predict(request: ImageInferenceRequest):
    """
    This route uses a count blood cells model to detect 3 types of blood cells in the given image,
    returns a list of the detected bounding boxes
    """
    if not request.imagePath:
        raise HTTPException(status_code=400,
                            detail="Please Provide a valid data file")

    output = cell_counter_model.infer(request.imagePath)
    return {'output': output}


@app.get("/convert-2-onnx")
def config_file():
    return {"message": "This route is not implemented yet."}


@app.post("/update-prod-model")
async def test_and_update_prod_model(model: bytes = File()):
    """
    This route will receive a model
    and will check if its a valid onnx then if it is, will update the current
    model running in production for the new one
    it will return a message regarding on the successfullness of the update
    """
    with open(f"model_config/model-{datetime.now()}.onnx", "wb") as new_model:
        new_model.write(model)

        new_model = ort.InferenceSession(model, providers = ['TensorrtExecutionProvider', 'CUDAExecutionProvider', 'CPUExecutionProvider'])

        cell_counter_model.update_model(new_model)

        return {"message": f"The production model is now the one located in {model}"}
