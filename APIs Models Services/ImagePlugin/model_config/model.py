import onnxruntime as ort
import numpy as np
import urllib.request as req
import numpy as np
import cv2
from darkflow.dark.darknet import Darknet
from darkflow.defaults import argHandler
from darkflow.net.framework import create_framework

class BloodCellsModel(object):

    def __init__(self, args):
        self._args = args
        # Load ONNX mode
        self._model = ort.InferenceSession(args.model_onnx, providers = ['CPUExecutionProvider'])
        self._input_name = self._model.get_inputs()[0].name
        self._label_name = self._model.get_outputs()[0].name
        self.version = args.model_version

        # Build framework to parse bboxes output
        self._options = {
            'model':args.model_cfg,
            'threshold': args.threshold,
            'labels': args.labels_path
        }
        new_options = argHandler()
        new_options.setDefaults()
        new_options.update(self._options)
        self._options = new_options
        darknet = Darknet(self._options)
        args_net = [darknet.meta, self._options]
        self._framework = create_framework(*args_net)

    def read_and_process(self, img_path):
        """
        Process bloodcell img so it can be inferenced by the count bloodcell model
        """
        file = req.urlopen(img_path)
        array = np.asarray(bytearray(file.read()), dtype=np.uint8)
        cells_img = cv2.imdecode(array, -1)
        cells_img = cv2.resize(cells_img, self._args.img_size)
        cells_img = np.expand_dims(cells_img.astype(np.single), axis=0)
        
        return cells_img

    def process_outputs(self, outputs):
        """
        Takes the output of the NN and makes it human readable
        """
        boxes = self._framework.findboxes(outputs[0][0,:,:,:])
        boxes = self._framework.findboxes(outputs[0][0,:,:,:])
        boxesInfo = list()
        for box in boxes:
            tmpBox = self._framework.process_box(box, self._args.img_size[0], self._args.img_size[1], self._options['threshold'])
            if tmpBox is None:
                continue
            boxesInfo.append({
                "label": tmpBox[4],
                "confidence": float(tmpBox[6]),
                "topleft": {
                    "x": int(tmpBox[0]),
                    "y": int(tmpBox[2])},
                "bottomright": {
                    "x": int(tmpBox[1]),
                    "y": int(tmpBox[3])}
            })
        return boxesInfo

    def infer(self, img_path):
        """
        Given an image url, read it, process it, feed forward through the network and makes the output human readable
        """
        cells_img = self.read_and_process(img_path)
        # Computes the prediction
        inputs = {self._input_name: cells_img}
        outputs = self._model.run(None, inputs)
        outputs = self.process_outputs(outputs)

        return outputs

    def update_model(self, new_model):
        """
        Update the onnx model instance this class uses
        """
        self._model = new_model