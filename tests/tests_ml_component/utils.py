import os
import requests
import json
import cerberus


def make_request(endpoint, type, obj = {}): #makes the request operations
    if type == 'GET':
        response = requests.get(endpoint) 
    elif type == 'POST':
        response = requests.post(endpoint, json=obj)

    try:
        return response.json(), response.status_code
    except:
        return {}, response.status_code


def make_request_onnx(endpoint, object):
    with open(object, 'rb') as f:
        file = {'model': f}
        response = requests.post(url=endpoint, files=file) 
        return response.json(), response.status_code
    
    
def validate_request(self): #take request and validate it by the schemas
    obj = {}
    with open(self.schema_file) as file:
        data = json.load(file)
        schema = data
        validator = cerberus.Validator(schema)

        if (self.object_file == "./objects/model2.onnx"):
            res, code = make_request_onnx(self.endpoint, self.object_file)
        else:
            if self.request_type == 'POST':
                with open(self.object_file) as f:
                    obj = json.load(f)
            res, code = make_request(self.endpoint, self.request_type, obj)

        if ("response" in self.metrics):
            print("RESPONSE FROM API")
            print(res)
            val = validator.validate(res,schema)
            if validator.errors != {}:
                print("ERRORS")
                print(validator.errors)
            else:
                print("Schema OK!")
            self.assertEqual(val,True)
        if ("code" in self.metrics):
            self.assertEqual(code,self.expected_code)
    return

def get_test_file_name(file):
    name = os.path.basename(file)
    name = name[5:]
    name = name[0:-3]
    return name
