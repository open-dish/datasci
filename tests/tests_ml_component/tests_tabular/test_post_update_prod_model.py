##########DONE##########

import os
import unittest
from utils import get_test_file_name, validate_request

class TestUpdate201(unittest.TestCase): #train in OK case

    def __init__(self, *args, **kwargs):
        super(TestUpdate201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/model2.onnx"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/update-prod-model"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["response", "code"]
        

    def test_request(self):
        validate_request(self)

