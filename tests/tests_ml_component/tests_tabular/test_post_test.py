#########DONE#########

import os
import unittest
from utils import get_test_file_name, validate_request

class TestTest201(unittest.TestCase): #test in 201 case

    def __init__(self, *args, **kwargs):
        super(TestTest201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/test_tabular_obj.json"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/test"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["response", "code"]
        

    def test_request(self):
        validate_request(self)

class TestTest400(unittest.TestCase): #test in 400 case

    def __init__(self, *args, **kwargs):
        super(TestTest400, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_none.json"
        self.object_file = "./objects/none_obj.json"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/test"
        self.request_type = "POST"
        self.expected_code = 400
        self.metrics = ["code"]
        

    def test_request(self):
        validate_request(self)
