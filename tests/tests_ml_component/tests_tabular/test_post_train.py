##########DONE##########
import os
import unittest
from utils import get_test_file_name, validate_request

class TestTrain201(unittest.TestCase): #train in OK case

    def __init__(self, *args, **kwargs):
        super(TestTrain201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/train_tabular_obj.json"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/train"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["response", "code"]
        

    def test_request(self):
        validate_request(self)

class TestTrain400(unittest.TestCase): #train in OK case

    def __init__(self, *args, **kwargs):
        super(TestTrain400, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_none.json"
        self.object_file = "./objects/none_obj.json"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/train"
        self.request_type = "POST"
        self.expected_code = 400
        self.metrics = ["code"]
        

    def test_request(self):
        validate_request(self)


