##########DONE##########
"""
import unittest
from utils import get_test_file_name, validate_request

class TestTrigger201(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestTrigger201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_none.json"
        self.object_file = "./objects/none_obj.json"
        self.endpoint = "https://run.mocky.io/v3/c6a68170-dc16-40fa-8bd0-8729e95e10c0"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["code"]
        

    def test_request(self):
        validate_request(self)
"""

