"""
import unittest
from utils import get_test_file_name, validate_request

class TestEvaluate201(unittest.TestCase): #train in OK case

    def __init__(self, *args, **kwargs):
        super(TestEvaluate201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/evaluate_tabular_obj.json"
        self.endpoint = "https://run.mocky.io/v3/d0e8bd6c-e843-49e2-a134-0c5a3245b08e"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["response", "code"]
        

    def test_request(self):
        validate_request(self)
"""