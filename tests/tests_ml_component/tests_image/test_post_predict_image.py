#############DONE##############
"""
import os
import unittest
from utils import get_test_file_name, validate_request

class TestPredict201(unittest.TestCase): #test in 201 case

    def __init__(self, *args, **kwargs):
        super(TestPredict201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/predict_tabular_obj.json"
        #self.endpoint = f"https://run.mocky.io/v3/eb0e0779-dabc-4b82-aeb0-67bc1d1c7761" #to run mock
        self.endpoint = f"http://{os.getenv('IMAGE_HOST')}/predict"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["response", "code"]
        

    def test_request(self):
        validate_request(self)

class TestPredict400(unittest.TestCase): #predict in 400 case

    def __init__(self, *args, **kwargs):
        super(TestPredict400, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_none.json"
        self.object_file = "./objects/none_obj.json"
        #self.endpoint = f"https://run.mocky.io/v3/eb0e0779-dabc-4b82-aeb0-67bc1d1c7761" #to run mock
        self.endpoint = f"http://{os.getenv('IMAGE_HOST')}/predict"
        self.request_type = "POST"
        self.expected_code = 400
        self.metrics = ["code"]
        

    def test_request(self):
        validate_request(self)
"""