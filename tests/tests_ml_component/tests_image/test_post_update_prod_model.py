##########DONE##########
"""
import os
import unittest
from utils import validate_request

class TestUpdate201(unittest.TestCase): #train in OK case

    def __init__(self, *args, **kwargs):
        super(TestUpdate201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_none.json"
        self.object_file = "./objects/none_obj.json"
        self.endpoint = f"http://{os.getenv('IMAGE_HOST')}/update-prod-model"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["code"]
        

    def test_request(self):
        validate_request(self)
"""
