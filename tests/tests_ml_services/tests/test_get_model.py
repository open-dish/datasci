import unittest
from utils import get_test_file_name, validate_request
import os

class TestModel(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestModel, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = f"http://{os.getenv('MANAGER_HOST')}/model-metadata/areas/audio/tasks/description/models/model1/1.1.1"
        self.request_type = "GET"
        self.expected_code = 200

    def test_schema(self):
        validate_request(self)
