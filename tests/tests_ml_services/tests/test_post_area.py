import unittest
from utils import get_test_file_name, validate_request
import os

class TestAddArea(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestAddArea, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = f"http://{os.getenv('MANAGER_HOST')}/model-metadata/areas"
        self.request_type = "POST"
        self.object_file = "./objects/post_area_obj.json"
        self.expected_code = 201
        
    def test_request(self):
        validate_request(self)
