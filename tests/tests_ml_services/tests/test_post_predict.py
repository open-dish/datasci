"""
import unittest
from utils import get_test_file_name, validate_request
import os


class TestAddPredict(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestAddPredict, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = f"http://{os.getenv('MANAGER_HOST')}/predict"
        self.request_type = "POST"
        self.object_file = "./objects/post_predict_obj.json"
        self.expected_code = 200
        
    def test_request(self):
        validate_request(self)
"""