import unittest
from utils import get_test_file_name, validate_request
import os

class TestAreas(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestAreas, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = f"http://{os.getenv('MANAGER_HOST')}/model-metadata/areas/"
        self.request_type = "GET"
        self.expected_code = 200

    def test_schema(self):
        validate_request(self)
