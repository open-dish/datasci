import unittest
from utils import get_test_file_name, validate_request
import os


class TestAddModel(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestAddModel, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = f"http://{os.getenv('MANAGER_HOST')}/model-saver/upload-model/areas/audio/tasks/description/models/model1"
        self.request_type = "POST"
        self.object_file = "./objects/post_model_obj.json"
        self.expected_code = 200
        
    def test_request(self):
        validate_request(self)
