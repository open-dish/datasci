import os
import requests
import json
import cerberus


def make_request(endpoint, tp, obj = {}):
    if tp == 'GET':
        response = requests.get(endpoint)
    elif tp == 'POST':
        response = requests.post(endpoint, json=obj)
    
    
    responseJson = response.json()
    print(responseJson)
    
    if type(responseJson) == list:
        responseJson = {"response": responseJson}

    try:
        return responseJson, response.status_code
    except:
        return {}, response.status_code


def validate_request(self):
    obj = {}
    if self.request_type == 'POST':
        with open(self.object_file) as f:
            obj = json.load(f)

    with open(self.schema_file) as file:
        schema = json.load(file)
        validator = cerberus.Validator(schema)
        response, code = make_request(self.endpoint, self.request_type, obj)
        val = validator.validate(response,schema)
        print("RESPONSE FROM API")
        print(response)
        if validator.errors != {}:
            print("ERRORS")
            print(validator.errors)
        else:
            print("Schema OK!")
        self.assertEqual(
            val,
            True
        )
        self.assertEqual(
            code,
            self.expected_code
        )


def get_test_file_name(file):
    name = os.path.basename(file)
    name = name[5:]
    name = name[0:-3]
    return name
