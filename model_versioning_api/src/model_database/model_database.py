from fastapi import APIRouter, HTTPException, UploadFile, File, Form, Response, status
from fastapi.responses import StreamingResponse
import os
import docker
from metadata_database.db import ModelMetadataDB
from sqlite3 import IntegrityError

# file to upload models to a database and get the model url

registry_username = os.environ.get("REGISTRY_UPLOAD_USERNAME")
registry_password = os.environ.get("REGISTRY_UPLOAD_PASSWORD")
registry_login_url = os.environ.get("REGISTRY_LOGIN_URL")
registry_upload_url = os.environ.get("REGISTRY_UPLOAD_URL")


router = APIRouter(
    prefix="/model-saver",
    tags=["model-saver"],
    responses={404: {"description": "Not found"}},

)


@router.post("/upload-model/areas/{area}/tasks/{task}/models/{model}", status_code=201)
async def upload_model(area: str, task: str, model: str,
                       version: str = Form(),
                       description: str = Form(),
                       input_format: str = Form(),
                       output_format: str = Form(),
                       url_production: str = Form(),
                       model_file: UploadFile = File(), 
                       response: Response = Response(status_code=status.HTTP_201_CREATED)):

    if None in [registry_username, registry_password, registry_login_url, registry_upload_url]:
        raise HTTPException(
            status_code=500, detail=f"Registry credentials not found")
    elif model_file.content_type != "application/x-tar":
        raise HTTPException(
            status_code=400, detail=f"Invalid model filetype: {model_file.content_type} - Needed .tar file")
    elif area not in [area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(
            status_code=400, detail=f"Nonexistent area: {area}")
    elif task not in [task["task"] for task in ModelMetadataDB.get_tasks(area=area)]:
        raise HTTPException(
            status_code=400, detail=f"Task {task} for area {area} nonexistent in database")
    elif model in [model["model"] for model in ModelMetadataDB.get_models(area=area, task=task, version=version)]:
        raise HTTPException(
            status_code=400, detail=f"Model {model}:{version} already in database")

    model_image_name = f"{registry_upload_url}/{model}"
    model_image_tag = version
    url_registry = f"{registry_upload_url}/{model}:{version}"

    docker_client = docker.DockerClient(base_url='unix://var/run/docker.sock')

    try:
        docker_client.login(username=registry_username,
                            password=registry_password,
                            registry=registry_login_url)

        docker_image = docker_client.images.load(model_file.file)[0]
        docker_image.tag(model_image_name, model_image_tag)

        registry_response = docker_client.api.push(
            model_image_name, model_image_tag)

        try:
            ModelMetadataDB.insert_model({"area": area,
                                          "task": task,
                                          "model": model,
                                          "version": version,
                                          "description": description,
                                          "input_format": input_format,
                                          "output_format": output_format,
                                          "url_production": url_production,
                                          "url_registry": url_registry})
            response.status_code = status.HTTP_201_CREATED
            return {"area": area,
                    "task": task,
                    "model": model,
                    "version": version,
                    "url_registry": url_registry}
        except IntegrityError as e:
            raise HTTPException(
                status_code=400, detail=f"SQLite Integrity Error: {e}")

    except docker.errors.APIError as e:
        print(e)
        raise HTTPException(
            status_code=503, detail="Internal Docker SDK failure")


@router.get("/fetch-model/areas/{area}/tasks/{task}/models/{model}/versions/{version}")
async def fetch_model(model: str, version: str, area: str, task: str):
    if None in [registry_username, registry_password, registry_login_url, registry_upload_url]:
        raise HTTPException(
            status_code=500, detail=f"Registry credentials not found")
    if model not in [model["model"] for model in ModelMetadataDB.get_models(model=model, version=version)]:
        raise HTTPException(
            status_code=400, detail=f"Model {model}:{version} nonexistent in database")
    model_entry = ModelMetadataDB.get_model_urls(
        area=area, task=task, model=model, version=version)
    url_registry = model_entry["url_registry"]

    docker_client = docker.DockerClient(base_url='unix://var/run/docker.sock')

    try:
        docker_client.login(username=registry_username,
                            password=registry_password,
                            registry=registry_login_url)

        docker_image = docker_client.images.pull(url_registry)

        return StreamingResponse(docker_image.save(chunk_size=None), headers={'Content-Disposition': f'attachment; filename="{model}_{version}.tar"'})

    except docker.errors.APIError:
        raise HTTPException(503)
