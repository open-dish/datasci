from fastapi import FastAPI
from model_handler import model_handler
from model_database import model_database
from metadata_database import db
# add cors to the api
from fastapi.middleware.cors import CORSMiddleware

# create the api


origins = [
    '*'
]


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(model_handler.router)
app.include_router(model_database.router)
app.include_router(db.router)


@app.get("/")
def read_root():
    return "Model Versioning API"
