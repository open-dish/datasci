# File to manage queries to the database


from metadata_database.db import ModelMetadataDB


def get_model_url(area: str, task: str, model: str, version: str) -> str:
    """
    This function is responsible for getting the url of the model from the
    database. The database is sqlite3. The database is stored in the
    model_versioning_api/model_versioning_api/model_database/model_database.db file.

    Parameters
    ----------
    area : str
        The area of the model. This is used to identify the model.
    task : str
        The task of the model. This is used to identify the model.
    model : str
        The name of the model. This is used to identify the model.
    version : str
        The version of the model. This is used to identify the model.

    Returns
    -------
    str
        The url of the model.

    """

    # using requests to make a get request to the database api

    model = ModelMetadataDB.get_model_urls(
        area=area, task=task, model=model, version=version)
    if model is None:
        return None

    url_production = model['url_production']

    return url_production
