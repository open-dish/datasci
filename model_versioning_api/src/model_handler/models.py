from pydantic import BaseModel
from typing import List
from uuid import UUID


class TabularModelResponse(BaseModel):
    """{
      "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
      "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
      "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
      "status": "final",
      "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
      "result": {
          "observation": [ { "category": "laboratory",
            "code": "718-7",
            "value": "15.1",
            "units": "g/dL",
            "type": "numeric"},],
      },
      "study":[
        "1d88a815-fbc2-9b61-b582-944100e41fd1"
      ],
      "conclusionCode": [
        2694001
      ]
      "realibility": [
        0.9
      ]
    }"""

    id: str
    patient: str
    encounter: str
    status: str
    resultsInterpreter: str
    result: dict[str,List[dict]]
    study: List[str]
    conclusionCode: List[int]
    reliability: List[float]


class ImageModelResponse(BaseModel):
  """{
      "label": "RBC",
      "confidence": 0.28805845975875854,
      "topleft": {
        "x": 0,
        "y": 0
      },
      "bottomright": {
        "x": 71,
        "y": 87
      }
    },"""
  label: str
  confidence: float
  topleft: dict[str,int]
  bottomright: dict[str,int]
