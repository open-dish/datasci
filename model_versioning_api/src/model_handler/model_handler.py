from fastapi import APIRouter,  HTTPException, Body
from .models import TabularModelResponse, ImageModelResponse
from .repository import get_model_url
import requests

# get from the env the link to the database with the response will be stored
import os


DATABASE_URL = os.environ.get("DATABASE_URL")


router = APIRouter(
    prefix="/model-handler",
    tags=["model-handler"]
)


"""
    This is the main router for the model versioning API. It is responsible for
    handling the requests and responses for the API. 

    The main objective of this API is to provide a way to call a model from  a
    specific version. This is done by providing the model name, version and
    input. The API will then return the output of the model, using the route /predict.

    POST /model-handler/areas/<area>/tasks/<task>/models/<model>/versions/<version>/predict



"""


@router.post("/areas/{area}/tasks/{task}/models/{model}/versions/{version}/predict")
async def predict_directioner(area: str, task: str, model: str, version: str, body_data: dict = Body(...)):


    """
    URL example: /areas/area1/tasks/task1/models/model1/versions/1.0.0/predict
    This function is responsible for handling the request and response for the
    API. It is responsible for calling the model and returning the output.

    Parameters
    ----------
    area : str
        The area of the model. This is used to identify the model.
    task : str
        The task of the model. This is used to identify the model.
    model : str
        The name of the model. This is used to identify the model.
    version : str
        The version of the model. This is used to identify the model.

    Returns
    -------
    dict
        The output of the model.

    """

    if area == "tabular":
        result_id = body_data['diagnostic']["id"]
    elif area == "image":
        result_id = body_data['image']['id']
        body_data = body_data['image']
    # Get the model url

    model_url = get_model_url(area, task, model, version)

    if model_url is None:
        raise HTTPException(
            status_code=404, detail="Model not found in database")

    # Call the model, using the model url, it's possible to make a request to the model and the data passed to the model through the request body

    # Get the output of the model
    output = get_prediction_from_model(model_url, body_data)

    if output is None:
        raise HTTPException(
            status_code=404, detail="Prediction could not be made")

    # Upload the data to the database
    response = upload_data_to_database_manegement(output, result_id, area)

    if response is None:
        raise HTTPException(
            status_code=404, detail="Data could not be uploaded to database")

    # return the id of the response

    return response['id']


def upload_data_to_database_manegement(data, result_id, area):
    """
    This function is responsible for uploading the data to the manegement database.
    There must be a call to the api that will store the data in the database, to make that
    call, the function requests.post is used with the database_url and the data as parameters.

    Parameters
    ----------
    data : dict
        The data to be uploaded to the database.

    Returns
    -------
    dict

    """
    try:
        if area == "tabular":
            data = TabularModelResponse(**data)
            URL_MANEGEMENT = DATABASE_URL + f"/diagnostic/{result_id}"
            data = data.dict()
        elif area == "image":
            data = [
                ImageModelResponse(**data) for data in data
            ]
            URL_MANEGEMENT = DATABASE_URL + f"/images/box/{result_id}"
        

        response = requests.put(URL_MANEGEMENT, json=data)
        response.raise_for_status()

    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)

    return response.json()


def get_prediction_from_model(model_url: str, input_data: dict):
    """get_prediction_from_model

    This function is responsible for calling the model and returning the output.

    Parameters
    ----------
    model_url : str
        The url of the model.

    Returns
    -------
    dict
        The output of the model.

    """
    URL_PREDICT = model_url + "/predict"

    try:
        response = requests.post(URL_PREDICT, json=input_data)
        response.raise_for_status()

    except requests.exceptions.HTTPError as errh:
        raise SystemExit(errh)

    except requests.exceptions.ConnectionError as errc:
        raise SystemExit(errc)

    except requests.exceptions.Timeout as errt:
        raise SystemExit(errt)

    except requests.exceptions.RequestException as err:
        raise SystemExit(err)
    finally:
        return response.json()
