from fastapi import APIRouter, HTTPException, Response, status
import sqlite3
import os
from .models import *


# file to deal with a sqlite database containing infos about the areas, models, etc

router = APIRouter(
    prefix="/model-metadata",
    tags=["model-metadata"],
    responses={404: {"description": "Not found"}},
)


class ModelMetadataDB:

    @staticmethod
    def create(db_path="model_metadata.db"):
        connection = sqlite3.connect(db_path)
        connection.execute("PRAGMA foreign_keys = 1")
        cursor = connection.cursor()

        cursor.execute("""
                       CREATE TABLE IF NOT EXISTS areas (
                           area        TEXT  PRIMARY KEY,
                           description TEXT
                       );
                       """)

        cursor.execute("""
                       CREATE TABLE IF NOT EXISTS tasks (
                           area          TEXT,
                           task          TEXT  PRIMARY KEY,
                           description   TEXT,
                           FOREIGN KEY(area) REFERENCES areas(area)
                       );
                       """)

        cursor.execute("""
                       CREATE TABLE IF NOT EXISTS models (
                           area  TEXT,
                           task  TEXT,
                           model TEXT,
                           version  TEXT,
                           description TEXT,
                           input_format  TEXT,
                           output_format TEXT,
                           url_registry  TEXT,
                           url_production  TEXT,
                           FOREIGN KEY(area) REFERENCES areas(area),
                           FOREIGN KEY(task) REFERENCES tasks(task),
                           PRIMARY KEY(model, version)
                       );
                       """)
        return connection

    @staticmethod
    def exists(db_path="model_metadata.db"):
        try:
            sqlite3.connect(f'file:{db_path}?mode=rw', uri=True)
            return True
        except sqlite3.OperationalError:
            return False

    @staticmethod
    def connect(db_path):
        connection = None
        if not ModelMetadataDB.exists(db_path):
            connection = ModelMetadataDB.create(db_path)
        else:
            connection = sqlite3.connect(db_path)
        connection.execute("PRAGMA foreign_keys = 1")
        return connection

    @staticmethod
    def build_variable_select_command(db_name, **parameters):
        command = f"SELECT * FROM {db_name}"
        if bool(parameters):
            command += " WHERE "
            for parameter in parameters:
                command += f"{parameter}=\"{parameters[parameter]}\" AND "
            command += "1"
        command += ";"

        return command

    @staticmethod
    def execute_insert_command(command, parameters=None, db_path="model_metadata.db"):
        connection = ModelMetadataDB.connect(db_path)
        cursor = connection.cursor()

        if parameters is None:
            cursor.execute(command)
        else:
            cursor.execute(command, parameters)

        connection.commit()
        return True

    @staticmethod
    def execute_delete_command(command, parameters=None, db_path="model_metadata.db"):
        connection = ModelMetadataDB.connect(db_path)
        cursor = connection.cursor()

        if parameters is None:
            cursor.execute(command)
        else:
            cursor.execute(command, parameters)

        connection.commit()
        return True

    @staticmethod
    def execute_update_command(command, parameters=None, db_path="model_metadata.db"):
        connection = ModelMetadataDB.connect(db_path)
        cursor = connection.cursor()

        if parameters is None:
            cursor.execute(command)
        else:
            cursor.execute(command, parameters)

        connection.commit()
        return True

    @staticmethod
    def execute_select_command(command, parameters=None, db_path="model_metadata.db"):
        connection = ModelMetadataDB.connect(db_path)
        cursor = connection.cursor()

        if parameters is None:
            cursor.execute(command)
        else:
            cursor.execute(command, parameters)

        return cursor.fetchall()

    @staticmethod
    def insert_area(area, db_path="model_metadata.db"):
        return ModelMetadataDB.execute_insert_command("""
                                          INSERT INTO areas(area, description)
                                          VALUES (:area, :description);
                                          """, area)

    @staticmethod
    def insert_task(task, db_path="model_metadata.db"):
        return ModelMetadataDB.execute_insert_command("""
                                          INSERT INTO tasks(area, task, description)
                                          VALUES (:area, :task, :description);
                                          """, task)

    @staticmethod
    def insert_model(model, db_path="model_metadata.db"):
        return ModelMetadataDB.execute_insert_command("""
                                          INSERT INTO models(area, task, model, version, description, input_format, output_format, url_production, url_registry)
                                          VALUES (:area, :task, :model, :version, :description, :input_format, :output_format, :url_production, :url_registry);
                                          """, model)

    @staticmethod
    def delete_model(model, db_path="model_metadata.db"):
        return ModelMetadataDB.execute_delete_command("""
                                          DELETE FROM models WHERE model=:model
                                                          AND area=:area 
                                                          AND task=:task 
                                                          AND model=:model 
                                                          AND version=:version;
                                          """, model)

    @staticmethod
    def update_model(model, db_path="model_metadata.db"):
        return ModelMetadataDB.execute_update_command("""
                                             UPDATE models SET area=:new_area,
                                                               task=:new_task,
                                                               model=:new_model,
                                                               version=:new_version
                                                           WHERE area=:area 
                                                                 AND task=:task 
                                                                 AND model=:model 
                                                                 AND version=:version;
                                          """, model)


    @staticmethod
    def get_areas(db_path="model_metadata.db", **parameters):
        command = ModelMetadataDB.build_variable_select_command(
            "areas", **parameters)
        areas = ModelMetadataDB.execute_select_command(command)

        return [{"area": area[0],
                 "description": area[1]} for area in areas]

    @staticmethod
    def get_tasks(db_path="model_metadata.db", **parameters):
        command = ModelMetadataDB.build_variable_select_command(
            "tasks", **parameters)
        tasks = ModelMetadataDB.execute_select_command(command)

        return [{"area":          task[0],
                 "task":          task[1],
                 "description":   task[2],
                 } for task in tasks]

    @staticmethod
    def get_models(db_path="model_metadata.db", **parameters):
        command = ModelMetadataDB.build_variable_select_command(
            "models", **parameters)
        models = ModelMetadataDB.execute_select_command(command)

        return [{"area":           model[0],
                 "task":           model[1],
                 "model":          model[2],
                 "version":        model[3],
                 "description":    model[4],
                 } for model in models]

    @staticmethod
    def get_model(db_path="model_metadata.db", **parameters):
        command = ModelMetadataDB.build_variable_select_command(
            "models", **parameters)
        model = ModelMetadataDB.execute_select_command(command)
        if len(model) == 0:
            raise HTTPException(status_code=400, detail=f"Model nonexistent in database")
        model = model[0]
        return {"area":           model[0],
                "task":           model[1],
                "model":          model[2],
                "version":        model[3],
                "description":    model[4],
                "input_format":   model[5],
                "output_format":  model[6],
                }

    @staticmethod
    def get_model_urls(db_path="model_metadata.db", **parameters):
        command = ModelMetadataDB.build_variable_select_command(
            "models", **parameters)
        model = ModelMetadataDB.execute_select_command(command)
        if len(model) == 0:
            raise HTTPException(status_code=400, detail=f"Model nonexistent in database")
        model = model[0]
        return {"url_registry":   model[7],
                "url_production": model[8]}

################## routes: ######################

@router.get("/areas/")
async def get_areas():
    return ModelMetadataDB.get_areas()


@router.get("/areas/{area}")
async def get_tasks(area: str):
    if area not in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Nonexistent area: {area}")
    task_info = {"area": area, "available-tasks": []}
    for task in ModelMetadataDB.get_tasks(area=area):
        task_info["available-tasks"].append(task["task"])
    return task_info


@router.get("/areas/{area}/{task}")
async def get_task_info(area: str, task: str):
    if area not in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Nonexistent area: {area}")
    elif task not in [ task["task"] for task in ModelMetadataDB.get_tasks(area=area)]:
        raise HTTPException(status_code=400, detail=f"Task {task} for area {area} nonexistent in database")
    return ModelMetadataDB.get_tasks(area=area, task=task)[0]


@router.get("/areas/{area}/{task}/models")
async def get_models(area: str, task: str):
    if area not in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Nonexistent area: {area}")
    elif task not in [ task["task"] for task in ModelMetadataDB.get_tasks(area=area)]:
        raise HTTPException(status_code=400, detail=f"Task {task} for area {area} nonexistent in database")
    models = ModelMetadataDB.get_models(area=area, task=task)
    return models


@router.get("/areas/{area}/{task}/models/{model}/{version}")
async def get_model(area: str, task: str, model: str, version: str):
    if model not in ModelMetadataDB.get_models(area=area, task=task, model=model, version=version):
        raise HTTPException(status_code=400, detail=f"Model nonexistent in database")
    model_entry = ModelMetadataDB.get_model(
        area=area, task=task, version=version)
    return model_entry


@router.delete("/areas/{area}/{task}/models/{model}/{version}")
async def delete_model(area: str, task: str, model: str, version: str):
    if area not in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Nonexistent area: {area}")
    elif task not in [ task["task"] for task in ModelMetadataDB.get_tasks(area=area)]:
        raise HTTPException(status_code=400, detail=f"Nonexistent task {task} for area {area}")
    elif model not in [ model["model"] for model in ModelMetadataDB.get_models(area=area, task=task, version=version)]:
        raise HTTPException(status_code=400, detail=f"Nonexistent model {model}:{version}")

    try:
        ModelMetadataDB.delete_model({"area": area,
                                      "task": task,
                                      "model": model,
                                      "version": version,
                                     })
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Unhandled exception: {e}")
    return {"result": "deleted"}


@router.put("/areas/{area}/{task}/models/{model}/{version}")
async def update_model(area: str, task: str, model: str, version: str, new_area: str, new_task: str, new_model: str, new_version: str):
    if area not in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Nonexistent area: {area}")
    elif task not in [ task["task"] for task in ModelMetadataDB.get_tasks(area=area)]:
        raise HTTPException(status_code=400, detail=f"Nonexistent task {task} for area {area}")
    elif new_area not in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Nonexistent area: {new_area}")
    elif new_task not in [ task["task"] for task in ModelMetadataDB.get_tasks(area=new_area)]:
        raise HTTPException(status_code=400, detail=f"Nonexistent task {new_task} for area {new_area}")
    elif model not in [ model["model"] for model in ModelMetadataDB.get_models(area=area, task=task, version=version)]:
        raise HTTPException(status_code=400, detail=f"Nonexistent model {model}:{version}")
    try:
        ModelMetadataDB.update_model({"area": area,
                                      "task": task,
                                      "model": model,
                                      "version": version,
                                      "new_area": new_area,
                                      "new_task": new_task,
                                      "new_model": new_model,
                                      "new_version": new_version,
                                     })
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Unhandled exception: {e}")
    return {"result": "updated"}

@router.post("/areas/", status_code=201)
async def post_area(body: PostAreaRequestBody, response: Response):
    if body.area in [ area["area"] for area in ModelMetadataDB.get_areas()]:
        raise HTTPException(status_code=400, detail=f"Area {body.area} already in database")
    ModelMetadataDB.insert_area(
        {"area": body.area, "description": body.description})
    response.status_code = status.HTTP_201_CREATED
    return {"area": body.area}


@router.post("/areas/{area}/tasks/{task}", status_code=201)
async def post_task(area: str, task: str, body: PostTaskRequestBody, response: Response):
    if task in [ task["task"] for task in ModelMetadataDB.get_tasks(area=area)]:
        raise HTTPException(status_code=400, detail=f"Task {task} for area {area} already in database")
    ModelMetadataDB.insert_task({"area": area,
                                 "task": task,
                                 "description": body.description,
                                 })
    response.status_code = status.HTTP_201_CREATED
    return {
        "area": area,
        "task": task
    }


def main():
    """
    (ModelMetadataDB.insert_area({"area": "audio", "description": "it is audio"}))
    (ModelMetadataDB.insert_area({"area": "text", "description": "it is text"}))

    (ModelMetadataDB.insert_task({"area": "text",
                              "task": "named entity recognition",
                              "description": "This task extracts the name entities from a text.",
                              "input_format": "text file; txt, etc.",
                              "output_format": "text file; txt, etc."}))
    (ModelMetadataDB.insert_task({"area": "audio",
                              "task": "audio thing",
                              "description": "This task does things with audio.",
                             }))

    (ModelMetadataDB.insert_model({"area": "text",
                               "task": "named entity recognition",
                               "model" : "BERT",
                               "version" : "1.0.0",
                               "description": "This model extracts the name entities from a text.",
                               "input_format": "text file; txt, etc.",
                               "output_format": "text file; txt, etc.",
                               "url_production": "http://lalala.com",
                               "url_registry": "http://google.com"}))

    (ModelMetadataDB.insert_model({"area": "audio",
                               "task": "audio thing",
                               "model" : "Whisper",
                               "version" : "1.0.2",
                               "description": "This model does things with audio.",
                               "input_format": "audio file; wav, etc.",
                               "output_format": "audio file; wav, etc.",
                               "url_production": "http://123123.com",
                               "url_registry": "http://bing.com"}))
    """

    print(ModelMetadataDB.get_areas())
    print(ModelMetadataDB.get_tasks())
    print(ModelMetadataDB.get_models(task="audio thing"))


if __name__ == "__main__":

    # create the database
    ModelMetadataDB.create()


# command to insert a models into models table

    """
    command = "INSERT INTO models VALUES ('audio', 'audio thing', 'Whisper', '1.0.2', 'This model does things with audio.', 'audio file; wav, etc.', 'audio file; wav, etc.', 'http://123123.com', 'http://bing.com')"
    """
