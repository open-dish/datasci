from pydantic import BaseModel


class PostAreaRequestBody(BaseModel):
    area: str
    description: str


class PostTaskRequestBody(BaseModel):
    description: str


class PostModelRequestBody(BaseModel):
    version: str
    description: str
    input_format: str
    output_format: str
    url_production: str
    url_registry: str
