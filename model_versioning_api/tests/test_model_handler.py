import sys
sys.path.append("../src")
from app import app
from model_database import *
from metadata_database import *
from model_handler import *
from fastapi.testclient import TestClient


test_image_data = {
    "image": {
        "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
        "date": "2003-11-01T20:30:34Z",
        "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
        "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
        "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
    }
}


test_tabular_data = {
    "patient": {
        "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
        "birthDate": "1998-05-25",
        "deathDate": "",
        "first": "Mariana",
        "last": "Rutherford",
                "race": "white",
                "ethnicity": "hispanic",
                "gender": "F",
                "birthPlace": "Yarmouth  Massachusetts  US",
                "lat": "42.63614335069588",
                "lon": "-71.3432549217789"
    },
    "diagnostic": {
        "_id": "6387ec1948af5115d39f2a44",
        "id": "f26d8606-63ce-dbbe-6571-5a056a361a97",
        "patient": "6a41d3c5-ebea-9824-b40f-594784ce9075",
        "encounter": "644c7c96-34cd-878f-a048-1456442fa943",
        "status": "final",
        "resultsInterpreter": "93ab5c5e-babb-3252-9bc3-d277b69ddcc4",
        "result": {
            "observation": []
        },
        "study": [
            ""
        ],
        "conclusionCode": [
            5251000175109,
            160903007,
            423315002,
            73595000
        ],
        "realibility": [
            1
        ]
    }
}

# this test will call the predict function from the model_handler.py file
# and will check if the output is the same as the expected output for images and tabular data


def test_predict(area):
    # using the server created in the app.py file
    client = TestClient(app)
    if area == "images":
        IMAGE_URL = "/model-handler/areas/images/tasks/object-detection/models/model1/versions/1/predict"
        body_data = test_image_data
        expected_output = [{
            "label": "RBC",
            "confidence": 0.28805845975875854,
            "topLeft": {
                "x": 0,
                "y": 0
            },
            "bottomRight": {
                "x": 71,
                "y": 87
            }
        }]
        response = client.post(IMAGE_URL, json=body_data)
        assert response.status_code == 200
        assert expected_output['id'] == response.json()['id']

    elif area == "tabular":
        
        TABULAR_URL = f"/model-handler/areas/tabular/tasks/anemia-detection/models/model1/versions/v1/predict"
        body_data = test_tabular_data
        expected_output = {
            "diagnostic": {
            "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
                           "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
                           "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
                           "status": "final",
                           "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
                           "result": {
                               "observation": [{"category": "laboratory",
                                                "code": "718-7",
                                                "value": "15.1",
                                                "units": "g/dL",
                                                "type": "numeric"}, ],
                           },
                           "study": [
                               "1d88a815-fbc2-9b61-b582-944100e41fd1"
                           ],
                           "conclusionCode": [
                               2694001
                           ],
                           "realibility": [
                               0.9
                           ],
                           }
        }

        response = client.post(TABULAR_URL, json=body_data)
        print(response.json())
        # assert response.status_code == 200
        # assert expected_output['id'] == response.json()['id']


test_predict(area="images")
